package dev.david.pruebas.holamundo2b50;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Iniciar la app de FireBase
        FirebaseApp.initializeApp(this);

        // Iniciar servicio de Auth
        mAuth = FirebaseAuth.getInstance();

        FirebaseUser usuario = this.mAuth.getCurrentUser();
        if(usuario != null){
            // Alguien ya inicio sesion
            Intent i = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
            return;
        }

        Button btnIngresar = findViewById(R.id.btIngresar);
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Crear la cuenta de usuario
                EditText etUsuario = findViewById(R.id.etUsuario);
                EditText etPass = findViewById(R.id.etPassword);

                String usuario = etUsuario.getText().toString();
                String pass = etPass.getText().toString();

                mAuth.signInWithEmailAndPassword(usuario, pass)
                        .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    Toast.makeText(MainActivity.this, "Bienvenido " +  user.getEmail(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });


            }
        });
    }

}








