package dev.david.pruebas.holamundo2b50;

import androidx.appcompat.app.AppCompatActivity;
import dev.david.pruebas.holamundo2b50.modelos.Producto;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore mDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        FirebaseApp.initializeApp(this);

        this.mAuth = FirebaseAuth.getInstance();
        this.mDB = FirebaseFirestore.getInstance();

        FirebaseUser usuario = this.mAuth.getCurrentUser();
        String email = usuario.getEmail();

        TextView tvUsuario = findViewById(R.id.tvUsuario);
        tvUsuario.setText(email);

        Button btGuardar = findViewById(R.id.btGuardar);
        btGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Guardar en la BD de Firebase
                Producto p = new Producto("marraqueta", Integer.parseInt("1890"));
                mDB.collection("productos").add(p);
                Toast.makeText(
                        HomeActivity.this,
                        "Producto creado",
                        Toast.LENGTH_SHORT
                ).show();
            }
        });

        Button btConsultar = findViewById(R.id.btConsultar);
        btConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDB.collection("productos").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<DocumentSnapshot> lista = queryDocumentSnapshots.getDocuments();
                        for(int i = 0; i < lista.size(); i++){
                            Producto p = lista.get(i).toObject(Producto.class);
                            Log.i("NUEVO_PROD", "PRODUCTO: NOMBRE: " + p.nombre + " PRECIO:" + p.precio);
                        }
                    }
                });
            }
        });

    }
}