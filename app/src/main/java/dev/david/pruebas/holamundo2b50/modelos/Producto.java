package dev.david.pruebas.holamundo2b50.modelos;

public class Producto {
    public String nombre;
    public int precio;

    public Producto(){

    }

    public Producto(String nombre, int precio){
        this.nombre = nombre;
        this.precio = precio;
    }
}
